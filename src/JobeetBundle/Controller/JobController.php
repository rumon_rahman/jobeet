<?php

namespace JobeetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JobeetBundle\Entity\Job;
use JobeetBundle\Form\JobType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Job controller.
 *
 */
class JobController extends Controller
{
    /**
     * Lists all Job entities.
     *
     * @Route("/", name="job_homepage")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //find all jobs
        //$jobs = $em->getRepository('JobeetBundle:Job')->findAll();

        // the latest active jobs
        /*
        $query = $em->createQuery('SELECT j from JobeetBundle:Job j WHERE j.expiresAt > :date')->setParameter('date', date('Y-m-d H:i:s', time() - 86400 * 30));
        $jobs = $query->execute();
        */

        // retrieve active jobs
        //$jobs = $em->getRepository('JobeetBundle:Job')->getActiveJobs();

        //retrieve jobs by category
        $categories = $em->getRepository('JobeetBundle:Category')->getWithJobs();



        foreach($categories as $category)
        {
            $category->setActiveJobs($em->getRepository('JobeetBundle:Job')->getActiveJobs(
                $category->getId(),
                $this->container->getParameter('max_jobs_on_homepage') //custom parameters from config.yml
            ));

            $category->setMoreJobs($em->getRepository('JobeetBundle:Job')->countActiveJobs($category->getId()) - $this->container->getParameter('max_jobs_on_homepage'));
        }

        return $this->render('job/index.html.twig', array(
            //'jobs' => $jobs,
            'categories' => $categories
        ));
    }

    /**
     * Creates a new Job entity.
     *
     * @Route("/new", name="job_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $job = new Job();
        $form = $this->createForm(new JobType(), $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            //file upload: we'll handle in Job model
            //$job->file->move(__DIR__.'/../../../web/uploads/jobs', $job->file->getClientOriginalName());
            //$job->setLogo($job->file->getClientOriginalName());

            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute('job_show', array(
                'company'=> $job->getCompanySlug(),
                'location' => $job->getLocationSlug(),
                'id' => $job->getId(),
                'position' => $job->getPositionSlug()
                ));
        }

        return $this->render('job/new.html.twig', array(
            'job' => $job,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Job entity.
     *
     * @Route(
     * "/{company}/{location}/{id}/{position}",
     * name= "job_show",
     * requirements= {"id" = "\d+"},
     * defaults={"id" = "1"}
     * )
     * @Method("GET")
     */
    public function showAction(Job $job)
    {
        //expired?
        if( $job->getExpiresAt() < new \DateTime('now') ){
            throw $this->createNotFoundException("Unable to find this job, may be expired?");
        }

        $deleteForm = $this->createDeleteForm($job);

        return $this->render('job/show.html.twig', array(
            'job' => $job,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Job entity.
     *
     * @Route("/{id}/edit", name="job_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Job $job)
    {
        $deleteForm = $this->createDeleteForm($job);
        $editForm = $this->createForm(new JobType(), $job);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute('job_edit', array('id' => $job->getId()));
        }

        return $this->render('job/edit.html.twig', array(
            'job' => $job,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Job entity.
     *
     * @Route("/{id}/delete", name="job_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Job $job)
    {
        $form = $this->createDeleteForm($job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($job);
            $em->flush();
        }

        return $this->redirectToRoute('job_index');
    }

    /**
     * Creates a form to delete a Job entity.
     *
     * @param Job $job The Job entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Job $job)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('job_delete', array('id' => $job->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
