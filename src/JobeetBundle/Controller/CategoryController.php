<?php

namespace JobeetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JobeetBundle\Entity\Category;
use JobeetBundle\Form\CategoryType;
use JobeetBundle\Entity\Job;


/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('JobeetBundle:Category')->findAll();

        return $this->render('category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_show', array('id' => $category->getId()));
        }

        return $this->render('category/new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     * @Route("/{slug}/{page}", name="category_show", defaults={"page" = "1"})
     * @Method("GET")
     */
    public function showAction($slug, $page)
    {
        //$deleteForm = $this->createDeleteForm($category);

        //get category from slug
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('JobeetBundle:Category')->findOneBySlug($slug);

        if(!$category){
            throw $this->createNotFoundException('Unable to find Category entity');
        }


        //per page
        $jobs_per_page = $this->container->getParameter('max_jobs_on_category');

        //get all jobs for retrieved category
        $category->setActiveJobs( $em->getRepository('JobeetBundle:Job')->getActiveJobs(
            $category->getId(),
            $jobs_per_page,
            ($page-1)*$jobs_per_page
        ));

        //classic pagination
        //total records number
        $total_jobs = $em->getRepository('JobeetBundle:Job')->countActiveJobs($category->getId());


        //how many page?
        $last_page = ceil($total_jobs/$jobs_per_page);

        $current_page = ($last_page == 1) ? 1: $page;

        //previous page number
        $previous_page = ($page > 1) ? $page-1 : 1;

        //next page number
        $next_page = ($page > $last_page)? $page : $page+1 ;

        return $this->render('category/show.html.twig', array(
            'category' => $category,
            'last_page'   =>  $last_page,
            'previous_page' => $previous_page,
            'current_page' => $current_page,
            'next_page'    => $next_page,
            'total_jobs' =>  $total_jobs,
        //    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm(new CategoryType(), $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_edit', array('id' => $category->getId()));
        }

        return $this->render('category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
