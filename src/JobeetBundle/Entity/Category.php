<?php

namespace JobeetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JobeetBundle\Utils\Jobeet;


/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="JobeetBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="JobeetBundle\Entity\Job", mappedBy="category")
     */
    private $jobs;

    /**
     * @ORM\ManyToMany(targetEntity="JobeetBundle\Entity\Affiliate")
     * @ORM\JoinTable(
     * joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")}
     * )
     * With a ManyToMany, you can optionally add a JoinTable annotation. Add this only if you want to customize
     * something about the join table.
     */
    private $affiliates;


    private $active_jobs;

    //The more_jobs property will hold the number of active jobs for the category minus the number of jobs listed on the homepage.
    private $more_jobs;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;



    /**
     * @return mixed
     */
    public function getMoreJobs()
    {
        return $this->more_jobs;
    }

    /**
     * @param mixed $more_jobs
     */
    public function setMoreJobs($more_jobs)
    {
        $this->more_jobs = $more_jobs >= 0? $more_jobs : 0;
    }

    /**
     * @return mixed
     */
    public function getActiveJobs()
    {
        return $this->active_jobs;
    }

    /**
     * @param mixed $active_jobs
     */
    public function setActiveJobs($active_jobs)
    {
        $this->active_jobs = $active_jobs;
    }

    /**
     * @return mixed
     */
    public function getAffiliates()
    {
        return $this->affiliates;
    }

    //Whenever we've a relationship that holds multiple things, we need to add a __construct method and
    //initialize it to an ArrayCollection
    public function __construct()
    {
        $this->affiliates = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString(){
        return $this->getName()? $this->getName() : "";
    }

    //get the slug of a category
    public function getSlug(){
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(){
        $this->slug = Jobeet::slugify($this->getName());
    }
}

