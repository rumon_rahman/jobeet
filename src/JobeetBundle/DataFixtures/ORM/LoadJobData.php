<?php
namespace JobeetBundle\DataFixtures\ORM;

// code: http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html#writing-simple-fixtures
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use JobeetBundle\Entity\Job;

class LoadJobData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $job1 = new Job();
        $job1->setCategory($em->merge($this->getReference('category-programming')));
        $job1->setType('full-time');
        $job1->setCompany('Sensio Labs');
        $job1->setLogo('sensio-labs.gif');
        $job1->setUrl('http://www.sensiolabs.com/');
        $job1->setPosition('Web Developer');
        $job1->setLocation('Paris, France');
        $job1->setDescription('You\'ve already developed websites with symfony and you want to work with Open-Source technologies. You have a minimum of 3 years experience in web development with PHP or Java and you wish to participate to development of Web 2.0 sites using the best frameworks available.');
         $job1->setHowToApply('Send your resume to fabien.potencier [at] sensio.com');
         $job1->setIsPublic(true);
         $job1->setIsActivated(true);
         $job1->setToken('job_sensio_labs');
         $job1->setEmail('job@example.com');
         $job1->setExpiresAt(new \DateTime('+30 days'));


         $job2 = new Job();
         $job2->setCategory($em->merge($this->getReference('category-design')));
         $job2->setType('part-time');
         $job2->setCompany('Extreme Sensio');
         $job2->setLogo('extreme-sensio.gif');
         $job2->setUrl('http://www.extreme-sensio.com/');
         $job2->setPosition('Web Designer');
         $job2->setLocation('Paris, France');
         $job2->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in.');
         $job2->setHowToApply('Send your resume to fabien.potencier [at] sensio.com');
         $job2->setIsPublic(true);
         $job2->setIsActivated(true);
         $job2->setToken('job_extreme_sensio');
         $job2->setEmail('job@example.com');
         $job2->setExpiresAt(new \DateTime('+20 days'));

        $job3 = new Job();
        $job3->setCategory($em->merge($this->getReference('category-programming')));
        $job3->setType('full-time');
        $job3->setCompany('EGov');
        $job3->setLogo('egov.jpg');
        $job3->setUrl('www.dpc.sa.gov.au/');
        $job3->setPosition('Web Developer');
        $job3->setLocation('Adelaide, Australia');
        $job3->setDescription('Are you drupal developer? Apply now.');
        $job3->setHowToApply('Send your resume to sami [at] dpc.com');
        $job3->setIsPublic(true);
        $job3->setIsActivated(true);
        $job3->setToken('job_dpc_drupal');
        $job3->setEmail('dpc@sa.gov.au');
        $job3->setExpiresAt(new \DateTime('+10 days'));

        $job4 = new Job();
        $job4->setCategory($em->merge($this->getReference('category-engineer')));
        $job4->setType('full-time');
        $job4->setCompany('Huawei Construction Ltd');
        $job4->setLogo('huwaei.jpg');
        $job4->setUrl('www.huawei.com.au/');
        $job4->setPosition('Civil Engineer');
        $job4->setLocation('Sydney, Australia');
        $job4->setDescription('The duties for this role will include planning, HSE, QA and all relevant paperwork etc required by the site based team to run projects smoothly and efficiently. You will ideally have experience in utilities / underground services and be highly organised with excellent computer skills.');
        $job4->setHowToApply('Send your resume to sami [at] huawei.com');
        $job4->setIsPublic(true);
        $job4->setIsActivated(true);
        $job4->setToken('job_huawei_civil');
        $job4->setEmail('abc@sa.gov.au');
        $job4->setExpiresAt(new \DateTime('+25 days'));

        $job5 = new Job();
        $job5->setCategory($em->merge($this->getReference('category-administrator')));
        $job5->setType('full-time');
        $job5->setCompany('EGov');
        $job5->setLogo('egov.jpg');
        $job5->setUrl('www.dpc.sa.gov.au/');
        $job5->setPosition('Content Editor');
        $job5->setLocation('Adelaide, Australia');
        $job5->setDescription('We\’re looking an stellar web content editor who can help us grow, who is a digital native and numbers person motivated by audience growth and incredible content creation. This is an exceptional opportunity to develop Refraction Media’s digital products across web, social, and newsletters and work on award-winning print and animation. You’ll need to be a fast-mover, a gun editor savvy across digital media platforms and bring ideas and a perfectionist attitude and be willing to learn across all aspects of an exciting start-up.');
        $job5->setHowToApply('Send your resume to sami [at] dpc.com');
        $job5->setIsPublic(true);
        $job5->setIsActivated(true);
        $job5->setToken('job_dpc_drupal_content');
        $job5->setEmail('dpc@sa.gov.au');
        $job5->setExpiresAt(new \DateTime('+30 days'));

        $job6 = new Job();
        $job6->setCategory($em->merge($this->getReference('category-manager')));
        $job6->setType('full-time');
        $job6->setCompany('EGov');
        $job6->setLogo('egov.jpg');
        $job6->setUrl('www.dpc.sa.gov.au/');
        $job6->setPosition('Manager, Web & Media');
        $job6->setLocation('Adelaide, Australia');
        $job6->setDescription('Are you PHP developer? Apply now.');
        $job6->setHowToApply('Send your resume to sami [at] dpc.com');
        $job6->setIsPublic(true);
        $job6->setIsActivated(true);
        $job6->setToken('job_dpc_web_manager');
        $job6->setEmail('dpc@sa.gov.au');
        $job6->setExpiresAt(new \DateTime('+5 days'));


        $job_expired = new Job();
        $job_expired->setCategory($em->merge($this->getReference('category-programming')));
        $job_expired->setType('full-time');
        $job_expired->setCompany('Sensio Labs');
        $job_expired->setLogo('sensio-labs.gif');
        $job_expired->setUrl('http://www.sensiolabs.com/');
        $job_expired->setPosition('Web Developer Expired');
        $job_expired->setLocation('Sydney, Australia');
        $job_expired->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit.');
        $job_expired->setHowToApply('Send your resume to lorem.ipsum [at] dolor.sit');
        $job_expired->setIsPublic(true);
        $job_expired->setIsActivated(true);
        $job_expired->setToken('job_expired');
        $job_expired->setEmail('job@example.com');
        $job_expired->setCreatedAt(new \DateTime('2015-10-01'));


        $job_cat = array('category-programming', 'category-design', 'category-manager', 'category-administrator', 'category-engineer', 'category-abc');

        for($i = 100; $i <= 200; $i++)
        {
            $job = new Job();

            $rand = array_rand($job_cat, 3);
            $job->setCategory($em->merge($this->getReference($job_cat[$rand[0]])));

            $job->setType('full-time');
            $job->setCompany('Company '.$i);
            $job->setPosition('Web Developer');
            $job->setLocation('Adelaide, Australia');
            $job->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit.');
            $job->setHowToApply('Send your resume to lorem.ipsum [at] dolor.sit');
            $job->setIsPublic(true);
            $job->setIsActivated(true);
            $job->setToken('job_'.$i);
            $job->setEmail('job@example.com');

            $em->persist($job);
        }


        $em->persist($job1);
        $em->persist($job2);
        $em->persist($job3);
        $em->persist($job4);
        $em->persist($job5);
        $em->persist($job6);
        $em->persist($job_expired);
        $em->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}


