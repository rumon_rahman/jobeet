<?php
namespace JobeetBundle\DataFixtures\ORM;

// code: http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html#writing-simple-fixtures
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use JobeetBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $design = new Category();
        $design->setName('Design');

        $programming = new Category();
        $programming->setName('Programming');

        $manager = new Category();
        $manager->setName('Manager');

        $admin = new Category();
        $admin->setName('Administrator');

        $engg = new Category();
        $engg->setName('Engineer');

        $em->persist($design);
        $em->persist($programming);
        $em->persist($manager);
        $em->persist($admin);
        $em->persist($engg);
        $em->flush();

        $this->addReference('category-design', $design);
        $this->addReference('category-programming', $programming);
        $this->addReference('category-manager', $manager);
        $this->addReference('category-administrator', $admin);
        $this->addReference('category-engineer', $engg);
    }

    public function getOrder()
    {
        return 1;
    }
}


