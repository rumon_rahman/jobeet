<?php
//By convention, the Tests/ subdirectory should replicate the directory of your bundle. So, when we are testing a class in our bundle’s Utils/ directory, we put the test in the Tests/Utils/ directory.

//run: phpunit -c app/ src/JobeetBundle/Tests/Utils/JobeetTest

namespace JobeetBundle\Tests\Utils;

use JobeetBundle\Utils\Jobeet;

class JobeetTest extends \PHPUnit_Framework_TestCase
{
    public function testSlugify()
    {
        $this->assertEquals('sensio', Jobeet::slugify('Sensio'));
        $this->assertEquals('sensio-labs', Jobeet::slugify('Sensio Labs'));
        $this->assertEquals('sensio-labs', Jobeet::slugify('sensio labs'));
        $this->assertEquals('paris-france', Jobeet::slugify('paris,france'));

        //test empty spaces
        $this->assertEquals('sensio', Jobeet::slugify(' sensio'));
        $this->assertEquals('sensio', Jobeet::slugify('sensio '));
        $this->assertEquals('n-a', Jobeet::slugify(''));
        $this->assertEquals('n-a', Jobeet::slugify(' - '));

        //transliteration: The test fails replacing é by e
        //  if (function_exists('iconv')) {
        //    $this->assertEquals('developpeur-web', Jobeet::slugify('Développeur Web'));
        //  }
    }
}


