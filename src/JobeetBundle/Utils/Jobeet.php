<?php
namespace JobeetBundle\Utils;


class Jobeet
{
    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('#[^\\pL\d]+#u', '-', $text);
        // trim
        $text = trim($text, '-');

        //var_dump(mb_detect_encoding($text)); die;

        // transliterate
        if (function_exists('iconv'))
        {
            //$text = utf8_encode($text);
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }



        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('#[^-\w]+#', '', $text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }
}